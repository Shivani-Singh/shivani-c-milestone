#include<stdio.h>

float PV(float rate,int np, float fv) {
    
    float power = 1;
    for(np;np>0;np--){
        power =power * (1+rate);
    }   
    float pv = fv / power;
    return pv;
}
 
int main()
{
    float fv,r,n;
	printf("Enter Future Value, Rate and Time: ");
	scanf("%f%f%f",&fv,&r,&n);
	float pv= PV(r,n,fv);	
	printf("Bank Loans Compound Interest = %f",pv);
	return 0;
	
}