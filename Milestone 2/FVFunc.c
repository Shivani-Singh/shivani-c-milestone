#include<stdio.h>

float FV(float rate,int np, float PV) {  
    float power = 1;
    for(np;np>0;np--){
        power =power * (1+rate);
    }   
    float FV = PV * power;
    return FV;
}
 
int main()
{
    float pv,r,n,fv;
    printf("Enter Present value, Rate and Time: ");
    scanf("%f%f%f",&pv,&r,&n);
    fv= FV(r,n,pv);	
    printf("Bank Loans Compound Interest = %f",fv);
    return 0;	
}