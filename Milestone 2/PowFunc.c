#include <stdio.h>

int power(int base,int exponent){
    int result=1;
    for(exponent;exponent>0;exponent--){
        result=result*base;
    }  
    return result;
}

int main()
{
    int base,exponent;
    printf("Enter base");
    scanf("%d",&base);
    printf("Enter exponent");
    scanf("%d",&exponent);
    int res=power(base,exponent);
    printf("Answer=%d",res);
    return 0;
}
