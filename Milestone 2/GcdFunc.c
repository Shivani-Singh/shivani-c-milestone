#include <stdio.h>

int gcd(int n1,int n2){
    int gcd,i;
    if(n1==0){
        gcd=n2;
    }
    if(n2==0){
        gcd=n1;
    }
    for(i=1;i<=n1 && i<=n2;i++){
    	if(n1%i==0&&n2%i==0)
		gcd=i;
    }
    return gcd;
}

int main(){
    int n1,n2;
    printf("print two numbers");
    scanf("%d%d",&n1,&n2);
    int res=gcd(n1,n2);
    printf("gcd=%d",res);
    return 0;
}