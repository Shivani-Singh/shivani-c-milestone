#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

int Length(struct List* head) {
    int count = 0;
    struct List* current = head;
    
    while(current != NULL){
        count++;
        current = current->next;
    }
    return(count);
}
  
void FrontBackSplit(struct List* source, struct List** front_ref, struct List** back_ref){
    
    int len = Length(source);
    int i;
    struct List* current = source;
    
    if(len<2){
        *front_ref = source;
        *back_ref = NULL;
    }
    else{
        int hopCount = (len-1)/2;
        for(i=0;i<hopCount;i++){
            current = current->next;
        }
        
        *front_ref = source;
        *back_ref = NULL;
    }
}

void push(struct List** b_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*b_ref); 
    (*b_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* source = NULL; 
    struct List* front = NULL;
    struct List* back = NULL;
    push(&source, 5); 
    push(&source, 2); 
    push(&source, 1);  
    push(&source, 3); 
    push(&source, 5);
    push(&source, 5);
    
    FrontBackSplit(source, &front, &back);
     
    printf("The source list is successfully splited into two list" ); 
} 