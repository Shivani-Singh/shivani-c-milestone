#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void RemoveDuplicates(struct List* head) {
    struct List* current = head;
    if(current  == NULL) return;
    
    while(current->next!=NULL){
        if(current->data == current->next->next){
            struct List* nextNext = current->next->next;
            free(current -> next);
            current->next = nextNext;
        }
        else{
            current = current->next;
        }
    }
        
}


void push(struct List** b_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*b_ref); 
    (*b_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1);  
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
    
    RemoveDuplicates(head);
     
    printf("All the duplicate node is deleted" ); 
} 