#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void MoveNode(struct List** dest_ref, struct List** source_ref){
    struct List* newNode = *source_ref;
    assert(newNode != NULL);
    *source_ref = newNode->next; 
    newNode->next = *dest_ref; 
    *dest_ref = newNode;
    
}

struct List* SortedMerge(struct List* a, struct List* b){
    struct List* result = NULL;
    struct List** lastPtrRef = &result;
 
    while (1) {
        if (a==NULL) {
            *lastPtrRef = b;
            break;
        }
        else if (b==NULL) {
            *lastPtrRef = a;
            break;
        }
        if (a->data <= b->data) {
            MoveNode(lastPtrRef, &a);
        }
        else {
            MoveNode(lastPtrRef, &b);
        }
        lastPtrRef = &((*lastPtrRef)->next); 
        
    }
    return(result);
}

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* a = NULL;
    struct List* b = NULL;
    struct List* head;
    
    push(&a, 3); 
    push(&a, 2); 
    push(&a, 1);  
    push(&b, 6); 
    push(&b, 5);
    push(&b, 4);
    head = SortedMerge(a,b);
    printf("The sorted Merge are ");
    
    while (head != NULL) { 
        printf("%d",head->data);
        head = head->next; 
    } 
   
} 