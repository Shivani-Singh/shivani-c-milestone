#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 
  
void SortedInsert(struct List** head_ref, struct List* newNode) 
{ 

   struct List** current_ref = head_ref;
   while(*current_ref != NULL && (*current_ref)->data < newNode->data){
       current_ref = &((*current_ref)->next);
   }
   newNode->next = *current_ref;
   *current_ref = newNode;
} 
  
void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1);  
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
     
    SortedInsert(&head,head);
     
    printf("The list is sorted" ); 
} 