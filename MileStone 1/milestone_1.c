// 1. A program to convert weight Entered in pounds(lbs) to kilogram(kg)

#include <stdio.h>

int main(void) {
  int a;
  float b;
  printf("Enter Weight in pounds");
  scanf("%d", &a);
  b = 0.453592 * a;
  printf("Weight in kilograms = %f", b);
  return 0;
}

// 2. A program to convert length Entered in inches to centimeters(cm)

#include <stdio.h>

int main(void) {
  int a;
  float b;
  printf("Enter length in inches");
  scanf("%d", &a);
  b = 2.54 * a;
  printf("lenght in centimeter = %.3f", b);
  return 0;
}

// 3. A program to convert temperature in Farenheit(F) to Celcius(C)

#include <stdio.h>

int main()
{
    float a;
    float b;
    printf("Enter temperature in Fahrenheit: ");
    scanf("%f", &a);
    b = (a - 32) * 5 / 9;
    printf("%.2f Fahrenheit = %.2f Celsius", a, b);
    return 0;
}

// 4. A program to calculate the Area of a Circle
#include <stdio.h>

int main(void) {
  int a;
  float b;
  const float pi= 3.14;
  printf("Enter radius");
  scanf("%d", &a);
  b = pi * a*a;
  printf("Area of circle = %.3f", b);
  return 0;
}

// 5. A program to calculate the Area of a Rectangle
#include <stdio.h>

int main(void) {
  int l,b, area;
  const float pi= 3.14;
  printf("Enter length and breadth");
  scanf("%d %d", &l, &b);
  area = l*b;
  printf("Area of Reactangle = %d", area);
  return 0;
}

// 6. A program to calculate the Area of a Triangle
#include <stdio.h>

int main(void) {
  int l,b, area;
  const float pi= 3.14;
  printf("Enter height and base");
  scanf("%d %d", &l, &b);
  area = 0.5*l*b;
  printf("Area of Tringle = %d", area);
  return 0;
}
